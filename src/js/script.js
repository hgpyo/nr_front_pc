var NR = {
	footer: function() {
		//푸터
		
		//가맹점문의 팝업
		$("#footer-partner").on("click", function() {
			NR.openPop($("#popup-footer-partner"));
			return false;
		});
		
		//제휴문의 팝업
		$("#footer-contact").on("click", function() {
			NR.openPop($("#popup-footer-contact"));
			return false;
		});

		//modify js for dev
		//제휴문의 팝업
		$("#footer-newstore").on("click", function() {
			NR.openPop($("#popup-footer-newstore"));
			return false;
		});
	},
	
	tab: function (){
		// $('.tab .tab-item').click(function(e){
			// e.preventDefault();
			// $(this).parents('li').addClass('active').siblings().removeClass('active');
		// });

		// 클릭 할 때 스크롤에 따른 탭버튼 활성화 막기 위함
		var flag = true;

		$(".tab .tab-item").on("click", function() {
			flag = false
			setTimeout(function(){
				flag = true;
			},1000)

			var getTab = $(this).closest(".tab");
			if(getTab.hasClass("tab-link")) return true;
			getTab.find("li.active").removeClass("active");
			var getIdx = $(this).parent().addClass("active").index();
			var getTabWrap = $(this).closest(".tab-wrap");
			if(getTabWrap.length > 0) {
				getTabWrap.find(".tab-area").removeClass("active").eq(getIdx).addClass("active");
			}
			if(getTab.hasClass("tab-move")) {
				var getPage = $(".tab-page").eq(getIdx);
				if(getPage.length > 0) {
					var getTop = getPage.offset().top - 57 - 131;
					$("html,body").stop(true,true).animate({ scrollTop:getTop }, 200);
				}
			}
			return false;
		});
		
		//베트스 카테고리
		$("#best-category-menu").on("click", ".category-menu .btn-tab", function() {
			$(this).parent().find(".active").removeClass("active");
			var getIdx = $(this).addClass("active").index();
			return false;
		}).on("click", ".category-list .btn-category", function() {
			var getIdx = $("#best-category-menu .category-list .btn-category").removeClass("active").index($(this).addClass("active"));
			return false;
		}).on("click", ".sort-menu .sort-list .btn", function() {
			$(this).parent().find(".active").removeClass("active");
			var getIdx = $(this).addClass("active").index();
			return false;
		});
		
		//탭 스크롤
		var tabScroll = $(".tab-scroll");
		if(tabScroll.length > 0) {
			var header = $("header");
			var tabPage = $('.product-detail-content .tab-page');
			$(window).on("scroll resize", function() {
				var getScroll = $(window).scrollTop() + 131;
				var getTabTop = tabScroll.offset().top;
				if(getScroll > getTabTop){
					tabScroll.addClass("active");
				}
				else {
					tabScroll.removeClass("active");
				}
				if(flag == true) {
					for(var i = 0; i < tabPage.length; i++) {
						var tabPageNum = tabPage.eq(i);
						var getScroll = $(window).scrollTop() + 196;
						var tabPageTop = tabPageNum.offset().top;
						var tabPageBottom = tabPageNum.offset().top + tabPageNum.outerHeight(true);

						if(getScroll > tabPageTop && getScroll < tabPageBottom ){
							$(".tab-scroll ul > li").eq(i).addClass("active");
						}else if( getScroll <= tabPage.eq(0).offset().top){
							$(".tab-scroll ul > li:first-child").addClass("active");

						}else {
							$(".tab-scroll ul > li").eq(i).removeClass("active");
						} 
					}
				}
			}).trigger("scroll");
		}

		//고객센터 1:1문의 빠른답변확인 팝업 탭
		$('.help-menu > li > a').click(function(e){
			$(this).addClass('active').parent('li').siblings().find('a').removeClass('active');
		})
	},
	
	select: function(target){
		if( target ){
		}
		$(".select-drop-down:not(.on)").each(function() {
			var getTarget = $(this).addClass("on");
			var getSelect = $(this).find("select");
			var getBtn = $(this).find(".btn-select");
			var getText = getSelect.find("option:selected").text();
			var getValue = getSelect.val();
			getBtn.removeClass("placeholder").html(getText);
			if(getValue == "") getBtn.addClass("placeholder");
			
			if(getSelect.is(":disabled")) {
				$(this).addClass("readonly");
				getBtn.on("click", function() {
					return false;
				});
				return true;
			}
			
			var getList = [];
			getSelect.find("option").each(function() {
				if(this.value !== "") {
					getList.push('<a href="#" data-value="' + this.value + '" class="btn-menu">' + $(this).text() + '</a>');
				}
			});
			$(this).append('<div class="select-drop-list">' + getList.join("") + '</div>');
			getBtn.on("click", function() {
				if(getTarget.hasClass("active")) {
					getTarget.removeClass("active");
				}
				else {
					$(".select-drop-down.active").removeClass("active");
					getTarget.addClass("active");
					$("body").off("click.selectdropdown").one("click.selectdropdown", function(e) {
						if($(e.target).closest(".select-drop-down").length == 0) {
							$(".select-drop-down.active").removeClass("active");
						}
					});
				}
				return false;
			});
			getTarget.find(".select-drop-list .btn-menu").on("click", function() {
				var setText = $(this).text();
				var setValue = $(this).data("value");
				getSelect.val(setValue).trigger("change");
				getBtn.removeClass("placeholder").html(setText);
				$(this).parent().find(".btn-menu").removeClass("active");
				$(this).addClass("active");
				getTarget.removeClass("active");
				$("body").off("click.selectdropdown");
				if(getTarget.hasClass("select-url") && setValue !== "") {
					location.href = setValue;
				}
				if( setValue == "selfType" ){
					$(this).parents('.filter-term').siblings('.date-picker').addClass('active');
				} else {
					$(this).parents('.filter-term').siblings('.date-picker').removeClass('active');
				}
				return false;
			});
		});
	},
	daterangepicker: function(){
		if( $('input[name="datepicker"]').length > 0){
			$('input[name="datepicker"]').daterangepicker({
				opens: 'left',
				locale: {
					"applyLabel": "적용",
					"cancelLabel": "취소",
					"weekLabel": "주",
					// "format": "MM/DD/YYYY",
					// "separator": " - ",
					// "fromLabel": "From",
					// "toLabel": "To",
					// "customRangeLabel": "Custom",
					"daysOfWeek": [
							"일",
							"월",
							"화",
							"수",
							"목",
							"금",
							"토"
					],
					"monthNames": [
							"1월",
							"2월",
							"3월",
							"4월",
							"5월",
							"6월",
							"7월",
							"8월",
							"9월",
							"10월",
							"11월",
							"12월"
					],
					"firstDay": 1
				}
			});
		}
	},
	
	check: function() {
		//전체 체크 선택
		$(".check-list-wrap").each(function() {
			var getList = $(this);
			var getCheckAll = $(this).find("input.check-all");
			var getCheckList = $(this).find("input.check:not(:disabled)");
			getCheckAll.on("change", function() {
				getCheckList.prop("checked", this.checked);
			});
			getCheckList.on("change", function() {
				getCheckAll.prop("checked", getList.find("input.check:not(:disabled):not(:checked)").length == 0);
			});
		});
	},
	
	tooltip: function() {
		$(".tooltip-drop-down").on("click", function() {
			$(this).toggleClass("active");
			return false;
		});
		
		$("body").on("click", "[data-tooltip]", function() {
			$(this).toggleClass("active");
			if($(this).hasClass("active")) {
				$(this).append('<div class="tooltip-popup">' + $(this).data("tooltip") + '</div>');
			}
			else {
				$(this).find(".tooltip-popup").remove();
			}
			return false;
		}).on("click", ".tooltip-popup", function() {
			$(this).parent().removeClass("active");
			$(this).remove();
			return false;
		});
	},
	
	accordion: function (){
		$("#accordion-list .accordion-title.show").removeClass("show").addClass("active").next(".accordion-content").show();
		$("#accordion-list .accordion-title").on("click", function() {
			if($(this).hasClass("active")) {
				$(this).removeClass("active").next(".accordion-content").slideUp(200);
			}
			else {
				$("#accordion-list .accordion-title.active").removeClass("active").next(".accordion-content").slideUp(200);
				$(this).addClass("active").next(".accordion-content").slideDown(200);
			}
			return false;
		});
	},
	location: function(){
		if( $('.location').length > 0 ){
			$('.location .toggle').click(function(e){
				e.preventDefault();
				if( $(this).parents('.has-combobox') ){
					$(this).parent('.has-combobox').toggleClass('active').siblings().removeClass('active');
				}
			});
			$(document).mouseup(function(e){
				if ( $(e.target).hasClass('toggle')){
					return false
				} else {
					$('.location .has-combobox.active').removeClass('active');
				}
			})

		}
	},
	
	file: function() {
		//첨부파일 선택
		$(".file-box .input-file").on("change", function() {
			var getFile = this.files[0];
			var getInput = $(this).parent().find(".input-text").val("");
			if(getFile !== undefined && getFile.name !== undefined && getFile.name !== "") {
				getInput.val(getFile.name);
			}
		});
	},
	
	form: function() {
		//키입력수 제한
		$(".input-text.input-limit").on("keyup", function() {
			var getText = this.value;
			var getCount = $($(this).data("for"));
			var getMaxlength = $(this).attr("maxlength");
			if(getCount.length > 0 && getMaxlength !== undefined) {
				var getLength = getText.length;
				if(getLength > getMaxlength) {
					getCount.html(getMaxlength);
					this.value = this.value.substr(0, getMaxlength);
				}
				else {
					getCount.html(getText.length);
				}
			}
		});

		
		$('.btn-drop-down').on('click',function(e){
			e.preventDefault();
			$(this).parents('.drop-down').toggleClass('active');
			$(this).parents('.menu-box').siblings().find('.drop-down').removeClass('active');
		});
		$('.drop-down-list a').on('click', function(e){
			e.preventDefault();
			var selected = $(this).text();
			var lists = $(this).parents('.drop-down-list');


			if( !lists.hasClass('label-fixed')){
				lists.prev('.btn-drop-down').text(selected);
			}
		})
		$(document).mouseup(function (e){
			var toggles = $('.btn-drop-down');
			if( $(e.target).hasClass('btn-drop-down') ){
				return false
			}else if( toggles.has(e.target).length === 0 && $(e.target).parents('.filter-type').length === 0 ){
				$('.drop-down').removeClass('active')
			}
		});
	},
	
	popup: function() {
		//팝업 공통
		//팝업 닫기
		$(".popup-window .popup-header .btn-close, .popup-window .popup-footer .btn-close").on("click", function() {
			NR.closePop($(this).closest(".popup-window"));
			return false;
		});
		$(".popup-window").on("click", function(e) {
			if($(e.target).hasClass("popup-window")) {
				NR.closePop($(e.target));
			}
		});
	},
	openPop: function(target){
		if(target !== undefined && target.length > 0) {
			$("body").addClass("popup-open");
			$(target).addClass("active").fadeIn(200);
		}
	},
	closePop: function(target){
		if(target !== undefined && target.length > 0) {
			$("body").removeClass("popup-open");
			$(target).removeClass("active").fadeOut(200);
		}
	},
	microPop: function(){
		if( $('.micro-popup').length > 0 ){
			$('.micro-popup .btn-close').click(function(e){
				e.preventDefault();
				$(this).parents('.micro-popup').remove();
			})
		}
	},
	videoPop: function(){
		if( $('.video-popup').length > 0 ){
			$('.video-popup .close-btn').click(function(e){
				// console.log();
				$(this).next('video').get(0).pause();
			});

			
		}
	},
	
	gnbScroll: function(){
		var last = 0;
		$(document).scroll(function(){
			var current = 0;
			current = $(document).scrollTop();
			if( last < current ){
				//down side
				if(current > 100 ){

					$('header').removeClass('active');

					if( $('.tab-scroll').length > 0 ){
						$('.tab-scroll').addClass('sticky');
					}

					
				}
			} else {
				// up side
				$('header').addClass('active');
				if( $('.tab-scroll').length > 0 ){
					$('.tab-scroll').removeClass('sticky');
				}
			}
			setTimeout(function(){
				last = current;
			}, 100);
			
			if(current > 100) {
				$("header").addClass("scroll");
			}
			else {
				$("header").removeClass("scroll");
			}
		});
	},
	gnbMegadrop: function(){
		if($(".header-mask").length == 0) return;
		
		var gnbCloseInterval = null;
		function gnbShow() {
			$(".header-mask, .header-bg, header .nav-btn").addClass("active");
		}
		function gnbClose() {
			$(".header-mask, .header-bg, header .nav-btn, .header-search").removeClass("active");
		}
		
		//기본 활성화 2뎁스 메뉴
		var getActiveSub = $("header .sub-list > li.active").addClass("open").eq(0);
		if(getActiveSub.length > 0) {
			var getActiveType = $(getActiveSub).data("type");
			$("header .sub-item-list .img-list").removeClass("active");
			$("header .sub-item-list .img-list[data-type='" + getActiveType + "']").addClass("active");
		}
		
		function gnbOpen(target) {
			clearTimeout(gnbCloseInterval);
			if($(target).hasClass("active")) return;
			
			gnbClose();
			$(target).addClass("active").siblings().removeClass("active");
			var getSub = $(target).find(".gnb-sub-menu");
			if(getSub.length > 0) {
				gnbShow();
				var getSubMenu = getSub.find(".sub-list > li.active").removeClass("active").end().find(".sub-list > li.open").addClass("active");
				if($("header .nav-list li:eq(0)").hasClass("active") && getSubMenu.length > 0) {
					var getActiveType = $(getSubMenu).data("type");
					$("header .sub-item-list .img-list").removeClass("active");
					$("header .sub-item-list .img-list[data-type='" + getActiveType + "']").addClass("active");
				}
			}
		}
		$("header .nav-list > li").hover(
			function() {
				gnbOpen(this);
			}
		);
		$("header").on("mouseleave", function() {
			$("header .nav-list > li.active").removeClass("active");
			gnbClose();
		});
		//키보드
		$("header .nav-list > li > a").on("focusin", function() {
			gnbOpen($(this).parent());
		});
		$("header").on("focusin", function() {
			clearTimeout(gnbCloseInterval);
		});
		$("body").on("focusin", function(e) {
			clearTimeout(gnbCloseInterval);
			if($(".header-mask").hasClass("active") && $(e.target).closest("header").length == 0) {
				gnbCloseInterval = setTimeout(function() {
					$("header .nav-list > li.active").removeClass("active");
					gnbClose();
				}, 300);
			}
		});
		
		function subOpen(target) {
			var getSubMenu = $(target).parent().addClass("active");
			getSubMenu.siblings().removeClass('active');
			if($("header .nav-list li:eq(0)").hasClass("active")) {
				var getActiveType = $(getSubMenu).data("type");
				$("header .sub-item-list .img-list").removeClass("active");
				$("header .sub-item-list .img-list[data-type='" + getActiveType + "']").addClass("active");
			}
		}
		$("header .sub-list > li > .btn-sub").hover(
			function() {
				subOpen(this);
			}
		);
		//키보드
		$("header .sub-list > li > .btn-sub").on("focusin", function() {
			clearTimeout(gnbCloseInterval);
			subOpen(this);
		});
		
		//검색
		function searchOpen() {
			clearTimeout(gnbCloseInterval);
			gnbClose();
			$(".header-mask, header .nav-btn, header .header-search").addClass("active");
			setTimeout(function() {
				$("header .header-search .header-input-search").focus();
			}, 100);
		}
		$("header .search-btn").on("click", function() {
			return false;
		});
		$("header .search-btn").on("mousedown", function (e) {
			$("header .nav-list > li.active").removeClass("active");
			if($("header .header-search").hasClass("active")) {
				gnbClose();
			}
			else {
				searchOpen();
			}
			return false;
		});
		//키보드
		$("header .search-btn").on("focusin", function() {
			searchOpen();
		});
		
		//닫기
		$("header .btn-gnb-close").on("click", function() {
			$("header .nav-list > li.active").removeClass("active");
			gnbClose();
		});
	},
	gnbAutoKeyword: function(){

		$('.input-search input').on('focus', function(){
			$(this).val("");
		});
		$('.input-search input').on('keyup', function(e){
			if( $(this).val().length > 0 ){
				$('.popular-keyword-list').hide();
				$('.recommand-keyword-list').show();
			} else {
				$('.popular-keyword-list').show();
				$('.recommand-keyword-list').hide();
			}
			
		})
	},
	
	slider: function() {
		//메인 배너 슬라이더
		if($(".main-bnr-slide").length > 0) {
			var mv = new Swiper($(".main-bnr-slide"), {
				autoplay: {
		          delay: 3000
		        },
				loop: true,
				speed: 600,
				centeredSlides: true,
				slidesPerView: "auto",
				spaceBetween: 100,
				pagination: {
		          el: ".swiper-pagination",
		          clickable: true,
							bulletClass: 'square-bullet',
		          renderBullet: function (index, className) {
		            return '<span class="' + className + '"></span>';
		          }
		        },
				navigation: {
    				prevEl: ".main-bnr-slide .btn-slider.slider-prev",
    				nextEl: ".main-bnr-slide .btn-slider.slider-next"
				},
				on :{
					slideChangeTransitionEnd: function(){
						var bg = document.querySelector('.swiper-slide-active').dataset.bg;
						$('.main-bnr-slide').css('background-color', bg);
					}
					
				}
			});
			$('.main-bnr-slide .swiper-stop').click(function(e){
				if( $(this).hasClass('stopped') ){
					mv.autoplay.start();
					$(this).removeClass('stopped');
				} else {
					mv.autoplay.stop();
					$(this).addClass('stopped');
				}
			});
		}
		
		//새상품 배너 슬라이더
		if($("#sub-bnr-slide").length > 0) {
			var slideItem = $("#sub-bnr-slide .bnr-slide-item");
			var sliderCount = $("#sub-bnr-slide-count .count");
			$("#sub-bnr-slide-count .total").html("/" + slideItem.length);
			new Swiper($("#sub-bnr-slide").get(0), {
				autoplay: false,
				speed: 600,
				navigation: {
    				prevEl: ".sub-bnr-wrap .btn-slider.slider-prev",
    				nextEl: ".sub-bnr-wrap .btn-slider.slider-next"
				},
				on: {
					init: function() {
					},
					slideChange: function() {
						sliderCount.html(this.realIndex + 1);
					}
				}
			});
		}
		
		//카테고리 배너 슬라이더
		if($("#category-bnr-slide").length > 0) {
			var slideItem = $("#category-bnr-slide .slide-item");
			var sliderCount = $("#category-bnr-slide-count .count");
			$("#category-bnr-slide-count .total").html("/" + slideItem.length);
			new Swiper($("#category-bnr-slide").get(0), {
				autoplay: false,
				speed: 600,
				slidesPerView: 3,
				spaceBetween: 25,
				navigation: {
    				prevEl: ".category-bnr-wrap .btn-slider.slider-prev",
    				nextEl: ".category-bnr-wrap .btn-slider.slider-next"
				},
				on: {
					init: function() {
					},
					slideChange: function() {
						sliderCount.html(this.realIndex + 1);
					}
				}
			});
		}
		
		//주문상세 최근 슬라이더
		if($(".recent-slide").length > 0) {
			$(".recent-slide").each(function(i) {
				var getId = "recent_slide_" + i;
				var getSlide = $(this).attr("id", getId);
				$(this).parent().attr("id", getId + "_wrap");
				new Swiper(getSlide.get(0), {
					autoplay: false,
					speed: 600,
					slidesPerView: 4,
					spaceBetween: 30,
					navigation: {
	    				prevEl: "#" + getId + "_wrap .btn-slider.slider-prev",
	    				nextEl: "#" + getId + "_wrap .btn-slider.slider-next"
					},
					on: {
						init: function() {
						}
					}
				});
			});
		}
		
		//상품상세 슬라이더
		if($("#product-img-slide").length > 0) {

			if($("#product-thumb-slide .swiper-slide").length < 5 ){
				$('#product-thumb-slide .btn-slider').remove();
			}
			var productThumbSlide = new Swiper($("#product-thumb-slide").get(0), {
				slidesPerView: 5,
				spaceBetween: 10,
				navigation: {
					prevEl: "#product-thumb-slide .btn-slider.slider-prev",
					nextEl: "#product-thumb-slide .btn-slider.slider-next"
				},
				allowTouchMove: false
			});
			var productImgSlide = new Swiper($("#product-img-slide").get(0), {
				autoplay: true,
				speed: 300,
				effect: 'fade',
				allowTouchMove: false,
				fadeEffect: {
				    crossFade: true
				},
				thumbs: {
		        	swiper: productThumbSlide
		        }
			});

			$('.product-thumb-slide .swiper-slide').on('click', function(){
				$('.product-img-top video').each(function(index, item){
					item.pause();
					$(item).siblings('.play-btn').removeClass('playing');
				})
			})
			
		}
		
		
		//상품상세 베스트리뷰 슬라이더
		if($("#review-detail-slide").length > 0) {
			var slideItem = $("#review-detail-slide .swiper-slide");
			var sliderCount = $("#review-detail-slide-count .count");
			$("#review-detail-slide-count .total").html("/" + slideItem.length);
			new Swiper($("#review-detail-slide").get(0), {
				autoplay: false,
				speed: 600,
				navigation: {
    				prevEl: ".review-detail-wrap .btn-slider.slider-prev",
    				nextEl: ".review-detail-wrap .btn-slider.slider-next"
				},
				on: {
					init: function() {
					},
					slideChange: function() {
						sliderCount.html(this.realIndex + 1);
					}
				}
			});
		}
		
		//주문상세 최근 슬라이더
		if($(".banner-slide-wrap").length > 0) {
			$(".banner-slide-wrap .brand-bnr-slide").each(function(i) {
				var getId = "banner_slide_" + i;
				var getSlide = $(this).attr("id", getId);
				$(this).parent().attr("id", getId + "_wrap");
				new Swiper(getSlide.get(0), {
					slidesPerView: 4,
					spaceBetween: 20,
					autoplay: false,
					speed: 600,
					navigation: {
	    				prevEl: "#" + getId + "_wrap .btn-slider-arrow.slider-prev",
	    				nextEl: "#" + getId + "_wrap .btn-slider-arrow.slider-next"
					},
					on: {
						init: function() {
						}
					}
				});
			});
		}
		
		//신상품 슬라이더
		if($("#na-bnr-slide").length > 0) {
			var na = new Swiper($("#na-bnr-slide"), {
				autoplay: {
		          delay: 3000
				},
				loop: true,
				speed: 600,
				centeredSlides: true,
				slidesPerView: "auto",
				// pagination: {
				// 		el: ".swiper-pagination",
				// 		clickable: true,
				// 		bulletClass: "btn-indicator",
				// 		renderBullet: function (index, className) {
				// 			return '<span class="' + className + '"></span>';
				// 		}
				// },
				

				pagination: {
					el: ".swiper-pagination",
					clickable: true,
					bulletClass: 'square-bullet',
					renderBullet: function (index, className) {
						return '<span class="' + className + '"></span>';
					}
				},
				navigation: {
						prevEl: ".main-bnr-slide .btn-slider.slider-prev",
						nextEl: ".main-bnr-slide .btn-slider.slider-next"
				}
			});
			$('.na-bnr-slide .swiper-stop').click(function(e){
				if( $(this).hasClass('stopped') ){
					na.autoplay.start();
					$(this).removeClass('stopped');
				} else {
					na.autoplay.stop();
					$(this).addClass('stopped');
				}
			});
		}
		
		//추천 신상품
		if($("#nr-bnr-slide").length > 0) {
			var slideItem = $("#nr-bnr-slide .swiper-slide");
			var sliderCount = $("#nr-bnr-slide-count .count");
			$("#nr-bnr-slide-count .total").html("/" + slideItem.length);
			var np = new Swiper($("#nr-bnr-slide").get(0), {
				slidesPerView: 4,
				spaceBetween: 20,
				autoplay: false,
				speed: 600,
				navigation: {
    				prevEl: "#nr-bnr-slide-wrap .btn-slider.slider-prev",
    				nextEl: "#nr-bnr-slide-wrap .btn-slider.slider-next"
				},
				on: {
					init: function() {
					},
					slideChange: function() {
						sliderCount.html(this.realIndex + 1);
					}
				}
			});
		}

		//이벤트 관련 상품 슬라이더
		if($(".event-bnr-wrap").length > 0) {
			new Swiper($(".event-bnr-wrap .brand-bnr-slide").get(0), {
				slidesPerView: 4,
				spaceBetween: 40,
				autoplay: false,
				speed: 600,
				navigation: {
						prevEl: ".event-bnr-wrap .btn-slider-arrow.slider-prev",
						nextEl: ".event-bnr-wrap .btn-slider-arrow.slider-next"
				},
				on: {
					init: function() {
					}
				}
			});
		}

		if($('.play-btn').length > 0){
			if( $('.play-btn').has)
			$('.play-btn').on('click', function(e){
				var video = $(this).siblings('video')[0];
				var playBtn = $(this);
				video.play();
				$(this).addClass('playing');
				video.addEventListener('ended', function(){
					playBtn.removeClass('playing');
				})
			})
		}

		if($('.remote-controls .list-wrap .swiper-slide').length > 3){
			$('.remote-controls .recent-list').addClass('over-three');
			new Swiper('.remote-controls .list-wrap', {
				direction: "vertical",
				slidesPerView: 3,
				navigation: {
          nextEl: ".recent-nav-next",
          prevEl: ".recent-nav-prev",
        }
			})
		}

	},
	
	category: function() {
		$("#category-menu .btn-menu").on("click", function() {
			var getParent = $(this).parent();
			if(getParent.hasClass("active")) {
				getParent.removeClass("active").find(".sub-list").slideUp(200);
			}
			else {
				$("#category-menu .menu-box").removeClass("active").find(".sub-list").slideUp(200);
				getParent.addClass("active").find(".sub-list").slideDown(200);
			}
			return false;
		});
	},
	
	cart: function() {
		
		//할인금액 드롭다운
		$(".order-payment-info .btn-detail-price").on("click", function() {
			$(this).parent().toggleClass("active").find(".info-detail-price").slideToggle(200);
			return false;
		});
		
		//결제수단 선택
		$(".order-payment-select .btn-payment").on("click", function() {
			$(this).parent().find(".btn-payment").removeClass("btn-fill black").addClass("btn-outline light-gray").end().find(".icon").removeClass("on");
			$(this).removeClass("btn-outline light-gray").addClass("btn-fill black").find(".icon").addClass("on");
			return false;
		});

		$('.order-payment-select a').click(function(e){
			var target = $(this).data('payment');
			$('.payment-notice .info-content.'+target).show().siblings().hide();
		});
		// $('.order-payment-select a').eq(0).click();
		
		// 상품상세 모달 토글
		if( $('.product-detail-top').length > 0 ){
			$('.product-opt-box .btn-opt').click(function(e){
				e.preventDefault();
				if($('.product-opt-box').hasClass('open')){
					var h = $('.product-opt-box').outerHeight();
					// $('.product-opt-box').css('transform', 'translateY(-'+h+'px)');
					$('.product-opt-box').removeClass('open');
				} else {
					$('.product-opt-box').css('transform', '');
					$('.product-opt-box').addClass('open');
				}

			})
		}

		//상품상세 모달
		if($("#product-opt-modal").length > 0) {
			var productOptModal = $("#product-opt-modal");
			var productOptBox = $(".product-detail-top");
			var getOptBottom = productOptBox.offset().top;
			
			$(window).on("scroll resize", function() {
				setTimeout(function(){
					var getScrollTop = $(document).scrollTop();
					var optHeight = productOptModal.outerHeight();
					var posTrigger = $('')
					if(getScrollTop > getOptBottom + optHeight ) {
						// 모달노출
						var h = $('.product-opt-box').outerHeight();
						productOptModal.addClass("sticky-bottom");
						$('.product-opt-box .select-drop-down').addClass('alignt');
						return false
					} else {
						// 모달숨김
						productOptModal.removeClass("sticky-bottom");
						$('.product-opt-box .select-drop-down').removeClass('alignt');
						$('.product-opt-box').css('transform', '');
						$('.product-opt-box').removeClass('open');
					}

				},300);
			}).trigger("resize");
		}
		
		//배송 요청사항
		if($("#select-shipping-request").length > 0) {
			$("#select-shipping-request").on("change", function() {
				$("#input-shipping-msg").hide();
				if(this.value === "msg") {
					$("#input-shipping-msg").show().find(".input-text").focus();
				}
			});
		}
	},
	
	// paymentNoticeTab: function(){
	// 	if( $('.order-payment-select').length > 0 ){
	// 		$('.order-payment-select a').click(function(e){
	// 			var target = $(this).data('payment');
	// 			$('.payment-notice .info-content.'+target).show().siblings().hide();
	// 		})
	// 	}
	// },
	
	review: function() {
		//리뷰 도움 버튼
		$(".btn-review-helpful").on("click", function() {
			//서버 연동
			if($(this).hasClass("green")) {
				$(this).addClass("light-gray").removeClass("green");
			}
			else {
				$(this).removeClass("light-gray").addClass("green");
			}
			return false;
		});
		
		//리뷰 팝업
		//임시 리뷰 팝업
		
		$(".product-detail-review .btn-review-link").on("click", function() {
			NR.openPop($("#popup-review-detail"));
			// NR.reviewSlide();
			return false;
		}).eq(0).trigger("click");
		
		//리뷰 작성
		//리뷰 점수
		$("#popup-review-write").on("click", ".point-box .icon", function() {
			$(this).parent().find(".icon").removeClass("on");
			$(this).addClass('on').prevAll(this).addClass("on");
			return false;
		});
		//리뷰 업로드
		$("#popup-review-write").on("change", ".write-picture-box .input-file", function() {
			var getFile = this.files[0];
			$(this).parent().removeClass("upload").find(".img").css("background-image", "none");
			if(getFile !== undefined && getFile.name !== undefined && getFile.name !== "") {
				var getImg = $(this).parent().addClass("upload").find(".img");
				var reader = new FileReader();
				reader.onload = function() {
				  	getImg.css("background-image","url('" + reader.result + "')");
				}
				reader.readAsDataURL(getFile);
			}
		}).on("click", ".write-picture-box .btn-close", function() {
			$(this).parent().removeClass("upload").find(".img").css("background-image", "none").end().find(".input-file").val("");
			return false;
		});
	},
	
	map: function() {
		//매장 찾기
		if($("#customer-map").length == 0) return;
		
		var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
		var options = { //지도를 생성할 때 필요한 기본 옵션
			center: new kakao.maps.LatLng(33.450701, 126.570667), //지도의 중심좌표.
			level: 3 //지도의 레벨(확대, 축소 정도)
		};
		
		var map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴

/* xxx, modify publisher js for finding store
		var markerPosition  = new kakao.maps.LatLng(33.450701, 126.570667);
		//var markerImage = new kakao.maps.MarkerImage("http://nature.afreehp.kr/src/img/icons/icon_map_marker.png", new kakao.maps.Size(51, 76));
		
		var imageSrc = "http://nature.afreehp.kr/src/img/icons/icon_map_marker.png";    
	    var imageSize = new kakao.maps.Size(51, 67);
		      
		var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize);
		
		var marker = new kakao.maps.Marker({
			map: map,
			image: markerImage,
		    position: markerPosition
		});
		
		var iwContent = '<div class="map-info-popup">' +
			                    '<div class="info-top"><p class="map-name">네이처리퍼블릭 본사</p><p class="map-address">서울특별시 강남구 테헤란로 534 글라스타워 22층,23층,24층(대치동)</p><p class="map-tel"><a href="tel:080-890-6000" class="btn-tel">080-890-6000</a></p><a href="" class="btn btn-sm btn-round btn-outline light-gray btn-store">매장사진</a></div>' +
			                    '<div class="info-transport">' +
			                        '<div class="bus-box list-box"><p class="label">버스</p><ul class="transport-list"><li class="blue"><p>145, 333, 341, 360, 730</p></li><li class="green"><p>4434, 41, 강남07</p></li><li class="red"><p>110 남양주, 1700 남양주, 2000 남양주, 007 남양주, 8001 남양주</p></li></ul></div>' +
			                        '<div class="subway-box list-box"><p class="label">지하철</p><ul class="transport-list"><li><p>2호선 삼성역 23,4번 출구</p></li></ul></div>' +
			                    '</div>' +
		                    '</div>';
	    var iwPosition = new kakao.maps.LatLng(33.450701, 126.570667); //인포윈도우 표시 위치입니다
		
		var infowindow = new kakao.maps.InfoWindow({
		    position: iwPosition, 
		    content: iwContent,
		    removable: true
		});
		  
		kakao.maps.event.addListener(marker, 'click', function() {
	    	infowindow.open(map, marker);  
	    	$(".map-info-popup").parent().prev().remove();
	    	$(".map-info-popup").parent().parent().addClass("map-info-wrap");
*/


		/***** s : DEV *****/		
		var searchlist = $('.result-list').eq(0).find('li');

		if(searchlist.length == 0){
		//	return;
		}

		var fcoords = new kakao.maps.LatLng(33.450701, 126.570667);
		
		$.each(searchlist, function(i,o){
			let target =  $(o).find('a').eq(0);

			let name = target.find('.map-name').html();
			let address = target.find('.map-address').html();
			let tel = $(o).find('.map-tel').eq(0).find('a').eq(0).html();
			let bustext = $(target).find('#bustext').val().split('\n');
			let metrotext = $(target).find('#metrotext').val().split('\n');
			let store_index = $(target).find('#store_index').val();
			let imgcount = $(target).find('#img_count').val();

			var busli = '';
			var metroli = '';

			var colarr = ['blue', 'green', 'red'];

			if(bustext.length >= 1 && (bustext[0])){

				$.each(bustext, function(i, o){
					busli += '<li class="'+colarr[i%3]+'"><p>'+o+'</p></li>'
				});

			}

			if(metrotext.length >= 1 && (metrotext[0])){

				$.each(metrotext, function(i, o){
					metroli += '<li class="'+colarr[i%3]+'"><p>'+o+'</p></li>'
				});

			}

			let xy = target.attr('data-latlng').split(',');

			var coords = new kakao.maps.LatLng(xy[0], xy[1]);

			if(i == 0){
				fcoords = coords;
			}

			var imageSrc = "/img/icons/icon_map_marker.png";    
			var imageSize = new kakao.maps.Size(51, 67);
				  
			var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize);
			
			var marker = new kakao.maps.Marker({
				map: map,
				image: markerImage,
				position: coords
			});

			var abtn = '<a href="javascript:void(0);" onclick="ews.review.imageview('+store_index+')" class="btn btn-sm btn-round btn-outline light-gray btn-store" data-idx="'+store_index+'">매장사진</a>';

			if(imgcount == 0){
				abtn = '';
			}

			kakao.maps.event.addListener(marker, 'click', function() {

				var iwContent = '<div class="map-info-popup">' +
										'<div class="info-top"><p class="map-name">'+name+'</p><p class="map-address">'+address+'</p><p class="map-tel"><a href="tel:'+tel+'" class="btn-tel">'
										+tel+'</a></p>' + abtn + '</div>' +
										'<div class="info-transport">' +
											'<div class="bus-box list-box"><p class="label">버스</p><ul class="transport-list">'+busli+'</ul></div>' +
											'<div class="subway-box list-box"><p class="label">지하철</p><ul class="transport-list">'+metroli+'</ul></div>' +
										'</div>' +
									'</div>';

				var iwPosition = new kakao.maps.LatLng(coords); //인포윈도우 표시 위치입니다
				
				var infowindow = new kakao.maps.InfoWindow({
					position: iwPosition, 
					content: iwContent,
					removable: true
				});

				$('.map-info-wrap').remove();
				infowindow.open(map, marker);  
				map.setCenter(coords);
				$(".map-info-popup").parent().prev().remove();
				$(".map-info-popup").parent().parent().addClass("map-info-wrap");
			});

			target.on('click', function(){
				var iwContent = '<div class="map-info-popup">' +
										'<div class="info-top"><p class="map-name">'+name+'</p><p class="map-address">'+address+'</p><p class="map-tel"><a href="tel:'+tel+'" class="btn-tel">'
										+tel+'</a></p>' + abtn + '</div>' +
										'<div class="info-transport">' +
											'<div class="bus-box list-box"><p class="label">버스</p><ul class="transport-list">'+busli+'</ul></div>' +
											'<div class="subway-box list-box"><p class="label">지하철</p><ul class="transport-list">'+metroli+'</ul></div>' +
										'</div>' +
									'</div>';


				var iwPosition = new kakao.maps.LatLng(coords); //인포윈도우 표시 위치
				
				var infowindow = new kakao.maps.InfoWindow({
					position: iwPosition, 
					content: iwContent,
					removable: true
				});

				$('.map-info-wrap').remove();
				infowindow.open(map, marker);  
				map.setCenter(coords);
				$(".map-info-popup").parent().prev().remove();
				$(".map-info-popup").parent().parent().addClass("map-info-wrap");
			});

		});

		map.setCenter(fcoords);
		
		//검색 팝업
		var mapSearchPopup = $("#map-search-popup").show();
		$(window).on("resize", function() {
			//xxx, modify publisher js for finding store
			/*
			var markerPosition = marker.getPosition(); 
			map.relayout();
			map.setCenter(markerPosition);
			*/
			
			var getMapHeight = $("#map").height() - 100;
			mapSearchPopup.height(getMapHeight);
		}).trigger("resize");
		/***** e : DEV *****/
		
		//검색 탭
		$("#map-search-popup .map-tab .btn-tab").on("click", function() {
			$(this).addClass("active").siblings().removeClass("active");
			$("#map-search-popup .map-tab-area").removeClass("active").eq($(this).index()).addClass("active");
			return false;
		});
		
		//더보기
		mapSearchPopup.find(".btn-result-close").on("click", function() {
			mapSearchPopup.toggleClass("close");
			return false;
		});
	},

	shareLinks: function(){
		if( $('.share-group').length > 0 ){
			$('.share-group .btn-share.toggle').click(function(e){
				e.preventDefault();
				$(this).parents('.share-group').toggleClass('active');
			})
			$(document).mouseup(function (e){
				var shareGroup = $('.share-group');
				if(shareGroup.has(e.target).length === 0){
					shareGroup.removeClass('active');
				}
			});
		}
	},

	mainTimeDiscount: function(){
		if( $('.time-discount .product-list').length > 0 ){
			$('.product-list .thumb-img').click(function(e){
				e.preventDefault();
				var targetProduct = $(this).data('target');
				$(this).addClass('active').siblings().removeClass('active');
				// $('.time-discount').css('background-image', 'url('+$(this).data('img')+')');
				$('.time-discount .product-info[data-product='+targetProduct+']').addClass('active').siblings().removeClass('active');
			})
		}
	},

	mainPopular: function(){
		//메인페이지 실시간인기상품 슬라이더
		if($(".popular-items-slide").length > 0) {
			var swiperSetting = function(e){
				var set = {
					autoplay: false,
					loop:true,
					slidesPerView: "auto",
					speed: 600,
					spaceBetween: 20,
					navigation: {
							prevEl: ".popular-items-slide .swiper-container[data-category='"+e+"'] .btn-slider-arrow.slider-prev",
							nextEl: ".popular-items-slide .swiper-container[data-category='"+e+"'] .btn-slider-arrow.slider-next"
					},
					on: {
						init: function() {
						}
					}
				}
				return set
			}
			// 스크롤바
			function swiperScrollbar(swiper, value){
				var totalSlide = swiper.loopedSlides;
				var oneclick = 100/totalSlide + '%'
				var drag = $(value).find('.swiper-scrollbar .scrollbar-drag');
				drag.css('width', oneclick);
				swiper.on('slideChange', function(){
					drag.css('transform', 'translate3d('+ 100*this.realIndex +'%,0,0)');
				});
			}

			$('.popular-items-slide .swiper-container').each(function(index, value){
				var currentSlide = $(value).data('category');
				var swp = new Swiper(value, swiperSetting(currentSlide));
				
				swiperScrollbar(swp, value);
			});

			//탭구현
			$('.wide-type-popular .tab .tab-item').click(function(e){
				var trg = $(this).data('target');
				$('.popular-items-slide .swiper-container[data-category="'+trg+'"]').addClass('active').siblings().removeClass('active');
			})
		}
	},
	

	mainRanking: function(){

		//메인 랭킹 이미지 슬라이더
		if($(".realtime-best-slide").length > 0) {
			var rankingRealtime = new Swiper($(".realtime-best-slide"), {
					autoplay: false,
					spaceBetween: 50,
					allowTouchMove: false,
					loop: true
			});
			$('.realtime-best a').click(function(e){
				e.preventDefault();
				$(this).parent('li').addClass('active').siblings().removeClass('active');
				var rank = $(this).data('rank');
				rankingRealtime.slideTo(rank);
			})
		}
		if($(".week-best-slide").length > 0) {
			var rankingWeek = new Swiper($(".week-best-slide"), {
					autoplay: false,
					spaceBetween: 50,
					allowTouchMove: false,
					loop: true
			});
			$('.week-best a').click(function(e){
				e.preventDefault();
				$(this).parent('li').addClass('active').siblings().removeClass('active');
				var rank2 = $(this).data('rank');
				rankingWeek.slideTo(rank2);
			})
		}

		//메인 랭킹 탭
		if( $('.ranking').length > 0 ){
			var tabSlide = $('.product-item-slide');
			$('.week-best-slide').hide();
			$('.ranking .tab a').click(function(e){
				e.preventDefault();
				var idx = $(this).data('idx');
				tabSlide.hide().eq(idx).show();
			})
		}
	},

	mainRecommand: function(){
		if( $('.recommand-slide').length > 0 ){
			var recommandSlider = new Swiper($(".recommand-slide"), {
				slidesPerView: 1,
        // spaceBetween: 30,
				navigation: {
					prevEl: ".recommand-wrap .btn-slider-arrow.slider-prev",
					nextEl: ".recommand-wrap .btn-slider-arrow.slider-next"
				},
				on :{
					slideChangeTransitionEnd: function(){
						var idx = this.activeIndex;
						$('.recommand-tab ul li').eq(idx).addClass('active').siblings().removeClass('active');
					}
				}
			});

			$('.recommand-tab .tab-item').click(function(e){
				var idx = $(this).parent('li').index();
				recommandSlider.slideTo(idx);
			})
		}
	},

	mainBrandlineSlider: function(){
		//카테고리 배너 슬라이더
		if($(".brand-line-slider").length > 0) {
			var slideItem = $(".brand-line-slider .slide-item");
			var sliderCount = $("#brand-slide-count .count");
			$("#brand-slide-count .total").html("/" + slideItem.length);
			new Swiper($(".brand-line-slider").get(0), {
				autoplay: false,
				speed: 600,
				slidesPerView: 3,
				spaceBetween: 15,
				navigation: {
    				prevEl: ".brand-list-wrap .btn-slider.slider-prev",
    				nextEl: ".brand-list-wrap .btn-slider.slider-next"
				},
				on: {
					init: function() {
					},
					slideChange: function() {
						sliderCount.html(this.realIndex + 1);
					}
				}
			});
		}
	},
	mainMdChoice: function(){
		if( $('.mdchoice-slide').length > 0 ){
			var mdchoiceSlider = new Swiper($(".mdchoice-slide"), {
				slidesPerView: 4,
        spaceBetween: 30,
				navigation: {
					prevEl: ".mdchoice-wrap .btn-slider-arrow.slider-prev",
					nextEl: ".mdchoice-wrap .btn-slider-arrow.slider-next"
				}
			});
		}
	},
	mainTopAnchor: function(){
		if( $('.remote-controls').length > 0 ){
			if( $('.brand-wrap').length > 0 ){
				$('.remote-controls').css('opacity', 0);
				$('.remote-controls').removeClass('active');
				return false
			} else {
				$(document).scroll(function(){
					var offTop = $(this).scrollTop();
					var offset = 300;
					if( offTop > 900 ){
						$('.remote-controls').css('display', 'inline-block');
						setTimeout(function(){
							$('.remote-controls').css('opacity', 1);
							$('.remote-controls').css('top', offTop+offset+'px');
							$('.remote-controls').addClass('active');
						},300);

					} else {
						
						$('.remote-controls').css('display', 'none');
						setTimeout(function(){
							$('.remote-controls').css('opacity', 0);
							$('.remote-controls').removeClass('active');
						},300);
					}
				})
			}
		} 
	},
	mainBannerPop: function(){
		if( $('.main-pop-slider').length > 0 ){
			var mbp = new Swiper($('.main-pop-slider'), {
				autoplay: false,
				speed: 600,
				pagination: {
					el: ".swiper-pagination",
					clickable: true,
					bulletClass: 'square-bullet',
					renderBullet: function (index, className) {
						return '<span class="' + className + '"></span>';
					}
				},
				navigation: {
    				prevEl: ".main-pop-slider .btn-slider.slider-prev",
    				nextEl: ".main-pop-slider .btn-slider.slider-next"
				},
				on: {
					init: function() {
					}
				}
			});
			$('.main-pop-slider .swiper-stop').click(function(e){
				console.log('클릭');
				if( $(this).hasClass('stopped') ){
					mbp.autoplay.start();
					$(this).removeClass('stopped');
				} else {
					mbp.autoplay.stop();
					$(this).addClass('stopped');
				}
			});
		}
	},
	scrollbarCustom: function(){
		$('.scroll-custom-y').scrollbar();
	},
	skintype: function(){
		if( $('.mypage-skininfo').length > 0 ){
			$('.mypage-skininfo > ul > li > a').click(function(e){
				e.preventDefault();
				$(this).toggleClass('off');
			})
		}
	},
	
	brand: function() {
		//브랜드 페이지
		if($(".brand-wrap").length > 0) {
			
			var brandVisual = $(".brand-visual");
			$(window).on("resize", function() {
				brandVisual.height($(window).height() - 100);
			}).trigger("resize");
			
			//스크롤 에니메이션
			var brandAnimate = $(".brand-animate");
			var scrollPrev = 0;
			var scrollDir = "dn";
			$(window).on("scroll", function() {
				var getTop = $(window).scrollTop();
				var getBottom = getTop + $(window).height();
				if(getTop !== scrollPrev) {
					if(scrollPrev < getTop) scrollDir = "dn";
					else if(scrollPrev > getTop) scrollDir = "up";
				}
				scrollPrev = getTop;
				
				brandAnimate.each(function() {
					var itemTop = $(this).offset().top;
					var itemBottom = itemTop + $(this).height();
					if(scrollDir == "dn") {
						if(itemTop < getBottom) {
							$(this).addClass("animated");
						}
					}
					else {
						if(itemTop > getBottom) {
							$(this).removeClass("animated");
						}
					}
				});
			}).trigger("scroll");

			$('header').addClass('trans')
		} else {

			$('header').removeClass('trans')
		}
		
		//브랜드 탭
		$(".brand-wrap .brand-menu .tab-item").on("click", function() {
			var getIdx = $(this).parent().index();
			$(".brand-wrap .brand-area").removeClass("active").eq(getIdx).addClass("active");
			if(getIdx == 1) {
				NR.brandMap(true);
			}
		});
		
		//역사
		if($(".brand-wrap .history-scroll").length > 0) {
			//년도 선택
			var historyScroll = $(".brand-wrap .history-scroll .scrollbar-inner");
			var historyPage = $(".brand-wrap .history-page .page-list .btn-year").on("click", function() {
				historyPage.removeClass("active");
				var getIdx = $(this).addClass("active").parent().index();
				if(historyList.eq(getIdx).length > 0) {
					var getTop = historyList.eq(getIdx).position().top + 10;
					historyScroll.animate({ scrollTop: getTop }, 200);
				}
				return false;
			});
			
			//스크롤
			var historyYear = $(".brand-wrap .history-list .year-box");
			$(".brand-wrap .history-year").append(historyYear);
			historyYear.eq(0).addClass("active");
			var historyList = $(".brand-wrap .history-list .history-info");
			$(".brand-wrap .scrollbar-inner").scrollbar({
				onScroll: function(x, y) {
					var getScrollTop = $(window).scrollTop();
					var getHistoryScrollTop = x.scroll;
					var getIdx = 0;
					historyList.each(function(key) {
						var getTop = $(this).position().top;
						if(getTop <= getHistoryScrollTop) {
							getIdx = key;
						}
					});
					historyYear.removeClass("active").eq(getIdx).addClass("active");
					historyPage.removeClass("active").eq(getIdx).addClass("active");
				}
			});
		}
		
		if($("#brand-intro-slide").length > 0) {
			new Swiper($("#brand-intro-slide").get(0), {
				autoplay: false,
				speed: 600,
				navigation: {
    				prevEl: ".brand-slide-area .btn-slide.prev",
    				nextEl: ".brand-slide-area .btn-slide.next"
				}
			});
		}
		
		if($("#brand-our-slide").length > 0) {
			new Swiper($("#brand-our-slide").get(0), {
				loop: true,
				autoplay: false,
				speed: 600,
				slidesPerView: 3,
				navigation: {
    				prevEl: ".our-slide-area .btn-slide.prev",
    				nextEl: ".our-slide-area .btn-slide.next"
				}
			});
		}
	},

	// 본사 찾아오는길 전용
	gotoMap: function(){
		var url = new URL(window.location.href);
		var urlParams = url.searchParams;
		if( urlParams.get('link') == 'gotoMap' ){
			$('html, body').css('scroll-behavior', 'initial');
			$('.brand-menu .tab .tab-item[data-tab="media"]').click();
			setTimeout(function(){
				var posMap = $('#brand-map').offset().top;
				$(window).scrollTop(posMap);
			},200);
		}
	},
	
	brandMap: function(type) {
		if(type == undefined && !$("#map").hasClass("init")) return;
		
		$("#map").addClass("init");
		var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
		var options = { //지도를 생성할 때 필요한 기본 옵션
			center: new kakao.maps.LatLng(37.508102225128, 127.06285388629), //지도의 중심좌표.
			level: 3 //지도의 레벨(확대, 축소 정도)
		};
		
		var map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴
		var markerPosition  = new kakao.maps.LatLng(37.508102225128, 127.06285388629);
		var imageSrc = "http://nature.afreehp.kr/src/img/icons/icon_map_marker.png";    
	    var imageSize = new kakao.maps.Size(51, 67);
		      
		var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize);
		
		var marker = new kakao.maps.Marker({
			map: map,
			image: markerImage,
		    position: markerPosition
		});
		
		//컨트롤 추가
		// var mapTypeControl = new kakao.maps.MapTypeControl();
		// map.addControl(mapTypeControl, kakao.maps.ControlPosition.TOPRIGHT);
		// var zoomControl = new kakao.maps.ZoomControl();
		// map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);

		setTimeout(function() {
			map.panBy(-150, -200);
			map.setZoomable(false);	//줌 막기
			map.setDraggable(false); //이동 막기
		}, 10);
		
		var iwContent = '<div class="map-info-popup">' +
			                    '<div class="info-top"><p class="map-name">네이처리퍼블릭 본사</p><p class="map-address">서울특별시 강남구 테헤란로 534 글라스타워 22층,23층,24층(대치동)</p><p class="map-tel"><a href="tel:080-890-6000" class="btn-tel">080-890-6000</a></p><a href="" class="btn btn-sm btn-round btn-outline light-gray btn-store">매장사진</a></div>' +
			                    '<div class="info-transport">' +
			                        '<div class="bus-box list-box"><p class="label">버스</p><ul class="transport-list"><li class="blue"><p>145, 333, 341, 360, 730</p></li><li class="green"><p>4434, 41, 강남07</p></li><li class="red"><p>110 남양주, 1700 남양주, 2000 남양주, 007 남양주, 8001 남양주</p></li></ul></div>' +
			                        '<div class="subway-box list-box"><p class="label">지하철</p><ul class="transport-list"><li><p>2호선 삼성역 23,4번 출구</p></li></ul></div>' +
			                    '</div>' +
		                    '</div>';
	    var iwPosition = new kakao.maps.LatLng(33.450701, 126.570667); //인포윈도우 표시 위치입니다
		
		var infowindow = new kakao.maps.InfoWindow({
		    position: iwPosition, 
		    content: iwContent,
		    removable: true
		});
	  	
		kakao.maps.event.addListener(marker, 'click', function() {
	    	infowindow.open(map, marker);  
	    	$(".map-info-popup").parent().prev().remove();
	    	$(".map-info-popup").parent().parent().addClass("map-info-wrap");
		});
		infowindow.open(map, marker);  
    	$(".map-info-popup").parent().prev().remove();
    	$(".map-info-popup").parent().parent().addClass("map-info-wrap");
	},

	
	orderPaymentSticky: function(){
		//주문결제 aside 스크롤시 sticky
		var offset = 354 - 70;
		var containerWidth = 1400;
		if( $('.order-payment-info').length > 0 ){

			$(document).scroll(function(e){
				// console.log($(this).scrollTop());
				var stopAt = $('footer')[0].offsetTop + 150 - $(window).height();
				if( $(this).scrollTop() > offset && $(window).width() > containerWidth ){
					$('.order-payment-info').addClass('sticky');
				} else {
					$('.order-payment-info').removeClass('sticky');
				}
				if( $(this).scrollTop() >  stopAt ){
					$('.order-payment-info').addClass('reach-bottom');
				} else {
					$('.order-payment-info').removeClass('reach-bottom');
				}
			})
		}
	},

	notibar: function() {
		//탑 노티 바
		$("body.notibar .header-notibar .btn-close").on("click", function() {
			// var notibar = $("body.notibar .header-notibar").animate({ height:0 }, 200);
			$('body.notibar').removeClass("notibar");
			$('.header-notibar').remove();
			return false;
		});
		if( $('body').hasClass('notibar') == true ){
			$(window).on('scroll', function(){
				var top = $(window).scrollTop();
				if( top > 100 && $('.header-notibar').length > 0 ){
					$('body').removeClass('notibar');
				} else if( top < 100 && $('.header-notibar').length > 0 ) {
					$('body').addClass('notibar');
				}
			})
		}
	},

	newArrival: function(){
		//스크롤 에니메이션
		var naAnimate = $(".na-animate");
		var scrollPrev = 0;
		var scrollDir = "dn";
		$(window).on("scroll", function() {
			var getTop = $(window).scrollTop();
			var getBottom = getTop + $(window).height();
			var getOffset = 500;
			if(getTop !== scrollPrev) {
				if(scrollPrev < getTop) scrollDir = "dn";
				else if(scrollPrev > getTop) scrollDir = "up";
			}
			scrollPrev = getTop;
			
			naAnimate.each(function() {
				var itemTop = $(this).offset().top;
				var itemBottom = itemTop + $(this).height();
				if(scrollDir == "dn") {
					if(itemTop < getBottom - getOffset) {
						$(this).addClass("animated");
					}
				}
				else {
					if(itemTop > getBottom - getOffset) {
						// $(this).removeClass("animated");
					}
				}
			});
		}).trigger("scroll");

		if($('.nr-tv-wrap .play-btn').length > 0){
			if( $('.play-btn').has)
			$('.play-btn').on('click', function(e){
				var video = $(this).siblings('video')[0];
				var playBtn = $(this);
				video.play();
				$(this).addClass('playing');
				video.addEventListener('ended', function(){
					playBtn.removeClass('playing');
				})
			})
		}
	},

	gotoReview: function(){
		if( $('.product-review-link').length > 0 ){
			$('.product-review-link .link-box a').click(function(e){
				e.preventDefault();
				//리뷰 탭으로 이동
				$('.product-detail-content .tab-item').eq(1).click();
			})
		}
	}
}

function init(){
	Object.keys(NR).forEach(function(fn){
		NR[fn]();
	})
}

$('document').ready(function(){
	init();
})


function TimeDiscountHour(setTime){
	var set = setTime;
	// 인증시간 카운트다운
	function timeTemplate(sec){
			var remainTime = sec / 1000;

			var time = "";
			var hour = Math.floor( remainTime / 3600 );
			remainTime -= 3600 * hour;
			if( hour < 10 ){
					hour = '0' + hour;
			}
			var minute = Math.floor( remainTime / 60 );
			remainTime -= 60 * minute;
			if( minute < 10 ){
					minute = '0' + minute;
			}
			var seconds = Math.floor( remainTime % 60 );
			if( seconds < 10 ){
					seconds = '0' + seconds;
			}

			var template_hour = '<span class="hour">'+ hour +'</span>';
			var template_min = '<span class="minute">'+ minute +'</span>';
			var template_sec = '<span class="second">'+ seconds +'</span>';

			time = template_hour + template_min + template_sec;
			return time
	}
	var interval = setInterval(function(){
			if( set < 1 ){
					clearInterval(interval);
			}                                                           
			var result = timeTemplate(set);
			$('.time-discount .time').html(result);
			set -= 1000; 
	}, 1000);
}

var yt;
	function ytVideoPopOpen(){
		console.log(yt);
		$('#videopopType01 .video-wrap').append(yt);
	}
	function ytVideoPopClose(){
		yt = $('#videopopType01 .video-wrap iframe').html();
		$('#videopopType01 .video-wrap iframe').remove();
	}