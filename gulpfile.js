'use strict';

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
// var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber');

// scss
var sass = require('gulp-sass')(require('sass'));

var departureSrc = './src/';
var destSrc = './src/';

// gulp.task('jsCompile', function (done) {
// 	// done();
// 	return gulp.src(departureSrc+'js/*.js') //경로 아래의 모든 js 파일을
// 		.pipe(plumber())
// 		.pipe(babel())
// 		.pipe(sourcemaps.write('./', {sourceRoot: '../src'}))
// 		.pipe(gulp.dest(destSrc+'js/dist')) //js 폴더에 저장
// 		.pipe(browserSync.stream());
// });

gulp.task('sass', function () {
	return gulp.src(departureSrc+'scss/**/*.scss')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({grid: true}))
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest(destSrc+'css'))
		.pipe(browserSync.stream());
});

gulp.task('serve', async function () {

	browserSync.init({
		server: './',
		port: 8081
	});

	gulp.watch([departureSrc+"scss/**/*.scss"], gulp.series('sass'));
	gulp.watch([departureSrc+"js/*.js"]);
	gulp.watch(departureSrc+"html/*.html").on('change', browserSync.reload);
});

gulp.task('default', gulp.series('sass', 'serve'));